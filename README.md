# CarCar

Team:

* Person 1 - Which microservice?

- Christopher Byrne: Sales

- Roman Badiere: Services
	-- branches (in chronological order to work flow)
		--services
		--1dayofpurepain
		--main (final configurations)


## Overview

CarCar: The only app you'll ever need for managing your dealership's inventory of automobiles.

## Diagram

Checkout this Diagram for a visual explanation of the CarCar App:
https://excalidraw.com/#room=94568acfa695de8606f4,BW-5AZWxk7JHLddHyw5dxQ


## How to Run This App

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository
https://gitlab.com/Byrne123/project-beta
2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/Byrne123/project-beta>>

3. Build and run the project using Docker with these commands:

```
docker volume create beta-data
docker-compose build
docker-compose up
```

- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/


## Design

CarCar is made up of 3 microservices which interact with one another.

- **Inventory**
- **Services**
- **Sales**

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

This all starts at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## API Documentation

**Manufacturers:**


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Volvo"
}
```
The return value of creating, viewing a single manufacturer:
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Volvo"
}
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Volvo"
    }
  ]
}
```
(Refer to diagram for further information)

**Vehicle Models:**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```
(Refer to diagram for further information)

**Automobiles:**

- The *'vin'* at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobiles Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
(Refer to diagram for further information)

# Sales Microservice

On the backend, the sales microservice has 4 models: AutomobileVO, Customer, SalesPerson, and Sales. Sales is the model that interacts with and gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.


## API Documentation:

**Customers:**


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name":"Martha",
	"last_name":"Stewart",
	"address":"1521 Walnut Street",
	"phone_number":"512-447-9974"
}
```
Return Value of Creating a Customer:
```
{
	"first_name": "Martha",
	"last_name": "Stewart",
	"address": "1521 Walnut Street",
	"phone_number": "512-447-9974",
	"id": 1
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"first_name": "Martha",
			"last_name": "Stewart",
			"address": "1521 Walnut Street",
			"phone_number": "512-447-9974",
			"id": 1
		},
		{
			"first_name": "Martha",
			"last_name": "Stewart",
			"address": "1521 Walnut Street",
			"phone_number": "512-447-9974",
			"id": 3
		}
	]
}
```
**Salespeople:**
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY):
-You must use a unique Employee_ID for each salesperson.

```
{
	"first_name": "Jon",
	"last_name": "Stewart",
	"employee_id": 4
}
```
Return Value of creating a salesperson:
```
{
	"salesperson": {
		"first_name": "Jon",
		"last_name": "Stewart",
		"employee_id": 16,
		"id": 16
	}
}
```
List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"first_name": "Jon",
			"last_name": "Stewart",
			"employee_id": 3,
			"id": 3
		},
		{
			"first_name": "BlueBlue",
			"last_name": "Rabbit",
			"employee_id": 777,
			"id": 5
		}
	]
}
```
**Sales:**
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/salesrecords/
| Create a new sale | POST | http://localhost:8090/api/salesrecords/
| Show salesperson's salesrecords | GET | http://localhost:8090/api/salesrecords/id/
List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"salesperson": {
				"first_name": "Jon",
				"last_name": "Stewart",
				"employee_id": 3,
				"id": 3
			},
			"customer": {
				"first_name": "Christopher",
				"last_name": "Byrne",
				"address": "2020 Blahsome drive",
				"phone_number": "512-447-9878",
				"id": 4
			},
			"automobile": {
				"vin": "JJJKKK",
				"sold": true
			},
			"price": 12345,
			"id": 26
		},
		{
			"salesperson": {
				"first_name": "Christopher",
				"last_name": "Hullaballoo",
				"employee_id": 7,
				"id": 13
			},
			"customer": {
				"first_name": "Julie",
				"last_name": "Gills",
				"address": "777 Angel drive",
				"phone_number": "512-459-2222",
				"id": 5
			},
			"automobile": {
				"vin": "Bloopy99",
				"sold": true
			},
			"price": 999,
			"id": 27
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
- Each sale must use a unique VIN from an unsold vehicle.
- If not using the browser forms, conveniently rendered for you, the customer, then you must refer to customers and salespeople using their database ID.

```
{
	"price": 50000,
    "customer": "1",
    "salesperson": "3",
    "automobile": "Bloopers"
}
```
Return Value of Creating a New Sale:
```
{
	"sale": {
		"salesperson": {
			"first_name": "Jon",
			"last_name": "Stewart",
			"employee_id": 3,
			"id": 3
		},
		"customer": {
			"first_name": "Martha",
			"last_name": "Stewart",
			"address": "1521 Walnut Street",
			"phone_number": "512-447-9974",
			"id": 1
		},
		"automobile": {
			"vin": "Bloopers",
			"sold": true
		},
		"price": 50000,
		"id": 32
	}
}
```

# Service microservice

(first and formost refer to diagram for relevant for example JSON strings)

**Technicians**
JSON GET, POST, PUT, DELETE Requests

**Service Appointments**
JSON GET, POST, PUT, DELETE requests

**Service microservice**
microservice in which records every appointed "Appointement" whether its finished, or canceled
the customers vehcile specifications (model, manufacturer, vin, reason)
who worked on the vehicle
and whether or not the customer had bought their vehcile from the Dealership using this website program

## Value Objects

**Sales**

**Service**


## Browser URLS


| Use |  URL
| ----------- | ----------- |
| List Manufacturers | http://localhost:3000/manufacturers
| Create Manufacturer | http://localhost:3000/createManufacturer
| Create Car Model | http://localhost:3000/createModel/
| List Car Models | http://localhost:3000/models/
| Add A Car | http://localhost:3000/addCar/
| List Car Inventory | http://localhost:3000/cars/
| Add A Technician | http://localhost:3000/addTechnician/
| List Technicians | http://localhost:3000/technicians/
| Create A Salesperson | http://localhost:3000/salespeople/create
| List Salespeople | http://localhost:3000/salespeople/
| Add A Customer | http://localhost:3000/customers/create/
| List Customers | http://localhost:3000/customers/
| Create A Sale | http://localhost:3000/sales/create/
| List Sales | http://localhost:3000/sales/
| Show Sales History | http://localhost:3000/sales_history/
| Create An Appointment | http://localhost:3000/createAppointment/
