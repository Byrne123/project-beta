import { useState, useEffect } from 'react';

function ListSalespeople() {
    const [salespeople, setSalespeople] = useState([]);


    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if(response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
        <h2>Salespeople</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map((person) => {
                    return(
                        <tr key={person.id}>
                            <td>{person.employee_id}</td>
                            <td>{person.first_name}</td>
                            <td>{person.last_name}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )
}

export default ListSalespeople
