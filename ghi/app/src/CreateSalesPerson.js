import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateSalesPerson() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const navigate = useNavigate();
    const [errorMessage, setErrorMessage] = useState('');


    const handleChange = (event, callback) => {
        const { target } = event;
        const { value } = target;
        callback(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const json = JSON.stringify(data);
        const url = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {method: 'POST', body: json, headers: {'Content-Type': 'application/json'}}

        const response = await fetch(url, fetchConfig);
        if(response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            navigate('/salespeople/');
        }
        else {
            console.error("Employee ID must be a unique integer.");
            setErrorMessage('Employee ID must be a unique integer.');
}
    }

    return(
        <div className="justify-content-center align-items-center">
            <div className='shadow p-4 mt-4'>
                <h1>Add a Salesperson</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input onChange={(event) => {handleChange(event, setFirstName)}} value={first_name} required placeholder="First name..." name="first_name" id="first_name" />
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={(event) => {handleChange(event, setLastName)}} value={last_name} required placeholder="Last name..." name="last_name" id="last_name" />
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={event => {handleChange(event, setEmployeeId)}} value={employee_id} required placeholder="Employee ID..." name="employee_id" id="employee_id" />
                    </div>
                    <div className="form-floating mb-3">
                        <button className="btn btn-primary">Create</button>
                        <div className="error"> {errorMessage}</div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateSalesPerson
