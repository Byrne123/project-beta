import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="/manufacturers">
                Manufacturers
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="/createManufacturer">
                Create Manufacturer
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="createModel/">
                Create Model
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="models/">
                Models
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="addCar/">
                Add Car
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="cars/">
                Car inventory
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="addTechnician/">
                Add Technician
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to="technicians/">
                Technicians
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/salespeople/create">
                Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/salespeople/">
                Salespeople
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/customers/create/">
                Add a Customer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/customers/">
                Customers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/sales/create/">
                Create a Sale
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/sales/">
                List of Sales
                </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" aria-current="page" to="/sales_history/">
                Sales History
                </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link active" aria-current="page" to="createAppointment/">
                Create Appointment
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link active" aria-current="page" to="appointments">
                Appointments
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link active" aria-current="page" to="serviceHistory">
                Service History
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
