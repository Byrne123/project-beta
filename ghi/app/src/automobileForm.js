import React, { useEffect, useState} from 'react';

function CreateModel () {

    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    const changeName = (event) =>{
        const value = event.target.value;
        setName(value);
    }
    const changePicture = (event) =>{
        const value = event.target.value;
        setPictureUrl(value);
    }
    const changeManufacturer = (event) =>{
        const value = event.target.value;
        setManufacturer(value);
    }

    const manufacturData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() =>{

        manufacturData();
    }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
        };

    };

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create Model</h1>
                    <form onSubmit={handleSubmit} id="create-carModel-form">
                        <div className='form-floating mb-3'>
                            <input value={name} onChange={changeName} placeholder='Name' required type='text' name = 'name' id='name' className='form-control' />
                            <label htmlFor='name'>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={pictureUrl} onChange={changePicture} placeholder='Picture Url' required type='text' name='picture_url' className='form-control'/>
                            <label htmlFor='picture_url'>Picture Url</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <select value={manufacturer} onChange={changeManufacturer} required id='manufacturer_id' name='manufacturer_id' className='form-select'>
                            <option>Choose a manufacturer</option>
                            {manufacturers.map(i => {
                                return (
                                    <option value={i.id} key={i.id}>
                                        {i.name}
                                    </option>
                                );
                            })}
                             </select>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
};


export default CreateModel
