import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom'

function AddTechnician(){

    const navigate = useNavigate()
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID ] = useState('')

    const changeFirst = event =>{
        const value = event.target.value;
        setFirstName(value)
    }
    const changeLast = event =>{
        const value = event.target.value;
        setLastName(value)
    }
    const changeEmployee = event =>{
        const value = event.target.value;
        setEmployeeID(value)
    }
    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const url = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type':'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
            navigate('/technicians');
        };
    };
    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className='form-floating mb-3'>
                            <input value={firstName} onChange={changeFirst} placeholder='First Name' required type='text' name='firstName' id='firstName' className='form-control'/>
                            <label htmlFor='firstName'>First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={lastName} onChange={changeLast} placeholder='Last Name' required type='text' name='lastName' id='lastName' className='form-control'/>
                            <label htmlFor='lastName'>Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={employeeID} onChange={changeEmployee} placeholder='EmployeeID' required type='text' name='employeeID' id='employeeID' className='form-control'/>
                            <label htmlFor='employeeID'>EmployeeID</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AddTechnician
