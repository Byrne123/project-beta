from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder

# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href",
    ]




@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            vin = content["vin"]
            if AutomobileVO.objects.filter(vin=vin).exists():
                content["vip"] = "YES"
            else:
                content["vip"] = "NO"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except (Technician.DoesNotExist, AutomobileVO.DoesNotExist):
            response = JsonResponse(
                {"message": "Technician or automobile does not exist"}
            )
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": f"Could not create the appointment: {str(e)}"}
            )
            response.status_code = 400
            return response
    else:
        response = JsonResponse(
            {"message": f"Method {request.method} not allowed"}
        )
        response.status_code = 405
        return response


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response


@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response


@require_http_methods(['GET', 'POST'])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": f"Could not create the technician: {str(e)}"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})



@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            vin = content["vin"]
            if AutomobileVO.objects.filter(vin=vin).exists():
                content["vip"] = "YES"
            else:
                content["vip"] = "NO"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except (Technician.DoesNotExist, AutomobileVO.DoesNotExist):
            response = JsonResponse(
                {"message": "Technician or automobile does not exist"}
            )
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": f"Could not create the appointment: {str(e)}"}
            )
            response.status_code = 400
            return response
    else:
        response = JsonResponse(
            {"message": f"Method {request.method} not allowed"}
        )
        response.status_code = 405
        return response


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response
